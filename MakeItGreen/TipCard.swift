//
//  SwiftUIView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 15.06.24.
//

import SwiftUI

struct TipCard: View {
    // Deklarieren der benötigten Variablen für die Erstellung der TipCard
    let tipHeading: String
    let tipDescription: String
    
    var body: some View {
        VStack(alignment: .leading){
            // Darstellen der Tip-Überschrift
            Text(tipHeading)
                .font(.title2).bold()
                .foregroundStyle(Color.primary)
                .multilineTextAlignment(.leading)
                .frame(alignment: .leading)
            // Darstellen der Tip-Beschreibung
            Text(tipDescription)
                .font(.title3)
                .foregroundStyle(.white)
                .multilineTextAlignment(.leading)
                .frame(alignment: .leading)
        }
        .padding()
        // Größe der Karte festlegen
        .frame(minWidth: 360, maxHeight: .infinity, alignment: .leading)
        //Hintergrund der Karte festlegen
        .background{
            RoundedRectangle(cornerRadius: 12)
                .foregroundStyle(Color.accentGreen)
                .opacity(0.50)
                .brightness(-0.2)
        }
    }
}

