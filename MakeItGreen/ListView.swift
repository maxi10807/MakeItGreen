//
//  ListView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI

// Hier wird festgelegt, wie die ActionCards sortiert werden sollen
enum SortOption: String, CaseIterable, Identifiable{
    var id: String {self.rawValue}
    case categorie = "Kategorie"
    case organizer = "Organisator"
}

struct ListView: View {
    // Festlegen der Sortier-Option
    @State private var selectedSortOption: SortOption = .categorie
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Variablen für Fehlermeldung
    @State private var alertMessage = ""
    @State private var showAlert = false
    
    var body: some View {
        NavigationStack() {
            VStack {
                // Auswahlmöglichkeit der beiden Sortier-Optionen
                Picker("Sortieren nach", selection: $selectedSortOption) {
                    ForEach(SortOption.allCases) { option in
                        Text(option.rawValue).tag(option)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding([.leading, .trailing])
                
                ScrollView() {
                    // Darstellen der eingeschriebenen Aktionen
                    ForEach(sortedData, id: \.id) { action in
                        // Link zur ActionView mit den passenden Daten
                        NavigationLink(destination: ActionView(actionID: action.id, onBack: {
                            dataManager.fetchUsers()
                            // Bei Rückkehr zur ParentView soll eine Meldung erfolgen, ob der Benutzer sich erfolgreich abgemeldet hat oder erfolgreich angemeldet
                            alertMessage = action.subscriber.contains(dataManager.uid) ? "Erfolgreich abgemeldet!" : "Erfolgreich angemeldet!"
                            showAlert = true
                        })) {
                            // Übergeben der Daten aus dem Action.Objekt
                            ActionCard(actionHeading: action.heading, actionCategorie: action.categorie, actionOrganizer: action.organizer, actionImage: action.image, actionLocation: action.location, actionSubcriberCount: action.subscriber.count)
                            // wenn Aktion älter als aktuelle Zeit ist, soll die Karte ausgegraut werden
                                .opacity(action.isPastTime() ? 0.3 : 0.8)
                        }
                    }
                }
                // View kann refresht werden, dabei sollen die Daten aktualisert werden
                .refreshable {
                    dataManager.fetchMyActions()
                }
                // wenn die View angezeigt wird, sollen auch die Daten aktualisert werden
                .onAppear{
                    dataManager.fetchMyActions()
               }
                .navigationTitle("Meine Aktionen")
                .tint(.accentGreen)
                // Erzeugen des passsenden Alerts
                .alert(isPresented: $showAlert, content: {
                    Alert(title: Text("Status"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                })
            }
        }
    }
    
    // Sortieren der Daten, jew nach ausgewählter Sortier-Option
    var sortedData: [Action]{
        switch selectedSortOption {
        case .categorie:
            return dataManager.myActions.sorted{$0.categorie < $1.categorie}
        case .organizer:
            return dataManager.myActions.sorted{$0.organizer < $1.organizer}
        }
    }
}

#Preview {
    ListView()
        .environmentObject(DataManager())
}
