//
//  Tip.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 20.06.24.
//

import SwiftUI

// Hier werden die benötigten Daten für ein Tip-Objekt deklariert
struct Tip: Identifiable {
    var id: String
    var heading: String
    var description: String
    var text: String
    var image: String
}
