//
//  Later.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 22.06.24.
//

import SwiftUI

struct Later: View {
    var body: some View {
        func saveAction(){
            let db = Firestore.firestore()
            let documentID = UUID().uuidString
            let heading:String = ""
            let organizer:String = ""
            let categorie:String = ""
            let image:String = ""
            let location:String = ""
            
            let newObject = Action(id: documentID, heading: heading, organizer: organizer, categorie: categorie, image: image, location: location)
            
            let objectData: [String:Any] = [
                "id":newObject.id,
                "heading":newObject.heading,
                "organizer":newObject.organizer,
                "categorie":newObject.categorie,
                "image":newObject.image,
                "location":newObject.location
            ]
            
            db.collection("actions").document(newObject.id).setData(objectData) { error in
                if let error {
                    print("Fehler beim Speichern des Dokuments: \(error.localizedDescription)")
                } else {
                    print("Dokument erfolgreich gespeichert mit ID: \(newObject.id)")
                }
            }
        }
    }
}

#Preview {
    Later()
}
