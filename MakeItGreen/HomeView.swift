//
//  HomeView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI

struct HomeView: View {
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    
    var body: some View{
        NavigationStack {
            ScrollView {
                VStack(spacing: 25) {
                    // Für jedes Objekt in dem tips-Array wird eine Card erzeugt
                    ForEach(dataManager.tips, id: \.id) { tip in
                        // Jede Karte hat ein Link zur TipView mit der passenden ID und dementsprechend werden die richtigen Informationen angezeigt
                        NavigationLink(destination: TipView(tipID: tip.id)) {
                            TipCard(tipHeading: tip.heading, tipDescription: tip.description)
                        }
                    }
                    Spacer()
                }
                .padding()
                .navigationTitle("Tipps")
            }
        }
    }
}

#Preview {
    HomeView()
}
