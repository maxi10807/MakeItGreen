//
//  ProfileView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI
import FirebaseAuth

struct ProfileView: View {
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    var profileImage: String = "profile" // Bildname in den Assets
    var body: some View {
        // erstes Objekt aus dem user-array mit der aktuellen UID des Nutzers
        if let user = dataManager.users.first(where: { $0.id == dataManager.uid}){
            VStack(spacing: 20){
                // Darstellen des Profilbildes
                Image(profileImage)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.accentGreen, lineWidth: 4))
                    .shadow(radius: 10)
                
                // Darstellen des Namens
                Text(user.username)
                    .font(.title)
                    .fontWeight(.bold)
                
                // Darstellen der Biografie
                Text(user.bio)
                    .font(.body)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal)
                
                // Darstellen des Standorts
                HStack {
                    Image(systemName: "location.fill")
                        .foregroundColor(.accentGreen)
                    Text(user.location)
                        .foregroundColor(.accentGreen)
                }
                .brightness(-0.2)
                // Darstellen von Kontaktinformationen
                VStack(spacing: 10) {
                    HStack {
                        Image(systemName: "envelope.fill")
                            .foregroundColor(.accentGreen)
                        Text(user.email)
                            .foregroundColor(.accentGreen)
                    }
                    .brightness(-0.2)
                    HStack {
                        Image(systemName: "phone.fill")
                            .foregroundColor(.accentGreen)
                        Text(user.tel)
                            .foregroundColor(.accentGreen)
                    }
                    .brightness(-0.2)
                }
                // Logout-Button, um den Account zu wechseln
                Button {
                    // Ausführen der SignOut-Methode aus FirebaseAuth
                    dataManager.signOut()
                } label: {
                    Text("Abmelden")
                        .bold()
                        .frame(width: 320, height: 40)
                        .foregroundStyle(.accentGreen)
                        .brightness(-0.2)
                        .background(
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .border(Color.black)
                        )
                }
                Spacer()
            }
        } else {
            // Wenn kein Benutzer gefunden wurde wird eine ProgressView angezeigt und die Daten der User neu gefetched
            ProgressView()
                .onAppear {
                    dataManager.fetchUsers()
                }
        }
    }
}


