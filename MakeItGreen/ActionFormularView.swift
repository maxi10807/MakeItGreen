//
//  ActionFormularView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 22.06.24.
//

import SwiftUI
import Firebase
import FirebaseFirestore

// Auswahl der Kategorie mit je passendem Bild
enum Categorie: String, CaseIterable, Identifiable{
    case Pflanzen, Sammeln, Aufbauen
    var id: Self {self}
    
    func displayImage()->String{
        switch self {
        case .Pflanzen:
            return "plant_tree"
        case .Aufbauen:
            return "saw_wood"
        case .Sammeln:
            return "pickup_trash"
        }
    }
}


struct ActionFormularView: View {
    // Enbinden DataManager
    @EnvironmentObject var dataManager: DataManager
    // Variablen für das Erstellen einer Aktion
    @State private var heading = ""
    @State private var image = ""
    @State private var location = ""
    @State private var categorie: Categorie = .Sammeln
    @State private var date = Date()
    @State private var items = ""
    @State private var description = ""
    // Fehlermeldung
    @State private var showAlert = false
    @State private var alertMessage = ""
    
    // Anpassung Textfelder
    init(){
        UITextField.appearance().tintColor = UIColor.accentGreen
        UITextView.appearance().tintColor = UIColor.accentGreen
    }
    
    var body: some View {
        // Feststellen des aktuellen Benutzers
        if let user = dataManager.users.first(where: { $0.id == dataManager.uid }) {
            NavigationView {
                // Erstellen eines Formulars
                Form {
                    Section(header: Text("Details")) {
                        // Eingabefelder
                        TextField("Titel", text: $heading)
                        TextField("Startort", text: $location)
                        // Date-Picker zur einfachen Auswahl des Datums und der Zeit
                        DatePicker("Datum und Uhrzeit", selection: $date, in: Date()...)
                        // Picker für die Kategorie
                        Picker("Kategorie", selection: $categorie){
                            Text("Sammeln").tag(Categorie.Sammeln)
                            Text("Aufbauen").tag(Categorie.Aufbauen)
                            Text("Pflanzen").tag(Categorie.Pflanzen)
                        }
                        TextField("Was wird benötigt?", text: $items)
                        // Vergrößertes Textfeld
                        TextField("Beschreibung", text: $description, axis: .vertical)
                            .lineLimit(5, reservesSpace: true)
                    }
                    
                    // Bei Klicken des Buttons soll ein Objekt aus den Variablen erstellt werden, mit einer eindeutigen UUID
                    Button(action: {
                        let action = Action(id: UUID().uuidString, heading: heading, organizer: user.username, categorie: categorie.rawValue, image: categorie.displayImage(), location: location, date: date, items: items, description: description, subscriber: [user.id])
                        // Aktion in Firestore schreiben
                        dataManager.addAction(action: action) { error in
                            if let error = error {
                                alertMessage = "Fehler beim erstellen: \(error.localizedDescription)"
                            } else {
                                // Erfolgreiche Rückmeldung und zurücksetzen der Variablen
                                alertMessage = "Aktion wurde erfolgreich erstellt"
                                heading = ""
                                image = ""
                                location = ""
                                date = Date()
                                categorie = .Sammeln
                                items = ""
                                description = ""
                            }
                            showAlert = true
                        }
                    }) {
                        Text("Aktion speichern")
                            .foregroundStyle(.accentGreen)
                            .brightness(-0.2)
                    }
                }
                .navigationBarTitle("Aktion erstellen")
                .alert(isPresented: $showAlert) {
                    Alert(title: Text("Aktion erstellt"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                }
            }
        } else {
            // Sonst wieder die Daten der User aktualiseren und neu versuchen
            ProgressView()
                .onAppear {
                    dataManager.fetchUsers()
                }
                
        }
    }
}
