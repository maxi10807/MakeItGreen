//
//  MakeItGreenApp.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI
import Firebase

@main
struct MakeItGreenApp: App {
    // Einbinden des DataManagers
    @StateObject var dataManager = DataManager()
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
  
    init() {
        // Firebase Verbindung starten
        FirebaseApp.configure()
    }
    var body: some Scene {
        WindowGroup {
            MainView()
                .environmentObject(dataManager)
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
   func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
       // Falls die Firebase Verbindung nicht richtig funktioniert hat, dann soll noch mal die Verbindung gestartet werden
    if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        return true
   }
}
