//
//  ActionData.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 17.06.24.
//

import SwiftUI

struct ActionData: Identifiable {
    var id = UUID()
    var image: String
    var categorie: String
    var heading: String
    var organizer: String
}

