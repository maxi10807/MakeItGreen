//
//  ContentView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI

struct ContentView: View {
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Veränderung an der Tabbar
    init(){
        UITabBar.appearance().backgroundColor = UIColor.accentGreen
        UITabBar.appearance().unselectedItemTintColor = UIColor.gray
        UITabBar.appearance().barTintColor = UIColor.accentGreen
        
    }
    var body: some View {
        // Erstellen einer TabView, welche zwischen mehreren View wechselt, mit einer konsistenten Tabbar
        TabView{
            // Hier werden die Tipps angezeigt
            HomeView()
                .tabItem {
                    Label("Tipps", systemImage: "house")
                }
            // Hier werden alle Aktionen angezeigt
            SearchView()
                .tabItem {
                    Label("Suchen", systemImage: "magnifyingglass")
                }
            // Hier kann eine neue Aktion erstellt werden
            ActionFormularView()
                .tabItem {
                    Label("Erstellen", systemImage: "plus.rectangle.fill")
                }
            // Hier werden alle beigetretenen Aktionen angezeigt
            ListView()
                .tabItem {
                    Label("Meine Aktionen", systemImage: "list.bullet")
                }
            // Hier wird das Profil angezeigt
            ProfileView()
                .tabItem {
                    Label("Profil", systemImage: "person")
                }
        }
    }
}

#Preview {
    ContentView()
        .environmentObject(DataManager())
}
