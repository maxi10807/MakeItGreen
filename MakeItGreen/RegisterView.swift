//
//  RegisterView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 23.06.24.
//

import SwiftUI

struct RegisterView: View {
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Deklarieren der benötigten Variablen, um einen Benutzer anzulegen
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var username: String = ""
    @State private var tel: String = ""
    @State private var location: String = ""
    @State private var bio: String = ""
    // Variable für Fehlermeldungen beim Registrieren
    @State private var errorMessage: String?
    
    // Anpassung der Textfelder
    init(){
        UITextField.appearance().tintColor = UIColor.accentGreen
        UITextView.appearance().tintColor = UIColor.accentGreen
    }
    
    var body: some View {
        // Hintergrund des Formulars für die Registrierung
        ZStack {
            // Farbverlauf zwischen zwei Punkten
            LinearGradient.linearGradient(colors: [.white, .green], startPoint: .bottomTrailing, endPoint: .topLeading)
            // Rechteck, mit entgegengesetzten Farbverlauf
            RoundedRectangle(cornerRadius: 30, style: .continuous)
                .foregroundStyle(.linearGradient(colors: [.accentGreen, .white], startPoint: .topLeading, endPoint: .bottomTrailing))
                .frame(width: 1000, height: 400)
                .rotationEffect(.degrees(135))
                .offset(y: -350)
            
            VStack(spacing: 20) {
                // Überschrift der View
                VStack(alignment: .leading) {
                    Text("Registrieren")
                        .foregroundStyle(.black)
                        .font(.largeTitle).bold()
                        .offset(y: -100)
                }
                .offset(x: -60)
                // Textfelder für die Eingabe der benötigten Parameter, .autocapitalization ist , damit der erste Buchstabe nicht groß ist
                TextField("Email", text: $email)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .autocapitalization(.none)
                
                TextField("Name", text: $username)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .autocapitalization(.none)
                // Eingabefeld für das Passwort, die Buchstaben tauche nicht in Klarsicht auf, sondern als Punkte
                SecureField("Passwort", text: $password)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .autocapitalization(.none)
                
                SecureField("Passwort bestätigen", text: $confirmPassword)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .autocapitalization(.none)
                
                TextField("Telefonnummer", text: $tel)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                
                TextField("Stadt", text: $location)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                
                TextField("Biografie", text: $bio)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                // Wenn es eine Fehlermeldung gibt, soll diese gezeigt werden
                if let errorMessage = errorMessage {
                    Text(errorMessage)
                        .foregroundColor(.red)
                        .padding()
                }
                
                // Button, um eine Registrierung durchzuführen
                Button(action: {
                    // Überprüfung, ob alle Felder ausgefüllt sind, sonst entsprechende Fehlermeldung geben
                    guard !email.isEmpty, !password.isEmpty, !confirmPassword.isEmpty, !username.isEmpty, !tel.isEmpty, !location.isEmpty, !bio.isEmpty
                    else {
                        errorMessage = "Alle Felder müssen ausgefüllt sein."
                        return
                    }
                    // Überprüfung, ob das Passwort übereinstimmt sonst entsprechende Fehlermeldung geben
                    guard password == confirmPassword else {
                        errorMessage = "Passwörter stimmen nicht überein."
                        return
                    }
                    // Überprüfung, ob das Passwort lang genug ist, sonst entsprechende Fehlermeldung geben
                    guard password.count >= 6 else {
                        errorMessage = "Das Passwort muss mindestens 6 Zeichen lang sein."
                        return
                    }
                    // Sonst keine Fehlermeldung geben
                    errorMessage = nil
                    // Ausführen der Register-Funktion
                    dataManager.register(email: email, password: password, username: username, tel: tel, location: location, bio: bio)}) {
                        // Aussehen des Buttons
                        Text("Registrieren")
                            .bold()
                            .frame(width: 320, height: 40)
                            .foregroundStyle(.accentGreen)
                            .brightness(-0.2)
                            .background(
                                RoundedRectangle(cornerRadius: 10, style: .continuous)
                            )
                    }
                    .padding()
            }
            .frame(width: 350)
            .padding()
        }
        .frame(height: 1000)
    }
}

#Preview {
    RegisterView()
        .environmentObject(DataManager())
}
