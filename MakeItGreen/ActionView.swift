//
//  ActionView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 16.06.24.
//

import SwiftUI

struct ActionView: View {
    // Einbinden einer Umgebung, um nachher aus der ChildView zurück zur ParentView zu kommen
    @Environment(\.presentationMode) var presentationMode
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Deklarieren der Aktions-ID , damit die richtigen Daten angezeigt werden
    var actionID: String
    //Deklarieren einer Variablen, welche im ParentView Funktionen ausführt
    var onBack: () -> Void
    // Variablen für die Fehlermeldung
    @State private var showAlert = false
    @State private var alertMessage = ""
    
    var body: some View {
        // Individueller Zurück-Button
        VStack{
            Spacer()
                .navigationBarBackButtonHidden(true)
                .toolbar(content: {
                    ToolbarItem(placement: .topBarLeading) {
                        Button(action: {
                            // Beim Drücken soll zum ParentView gewechselt werden
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Image(systemName: "arrow.left")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                            Text("Zurück")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                        })
                    }
                })
        }
        // Aktions-Objekt aus dem Aktions-Array soll die der mit geliferten ActionID übereinstimmen
        if let action = dataManager.actions.first(where: { $0.id == actionID}) {
            ScrollView {
                // Darstellen der Informationen
                VStack(alignment: .leading, spacing: 16) {
                    Image(action.image)
                        .resizable()
                        .frame(height: 200)
                        .cornerRadius(10)
                        .padding([.leading, .trailing], 16)
                    // Darstellen des Namens der Aktion
                    Text(action.heading)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding([.leading, .trailing], 22)
                    // Link zum Profil des Organisators
                    NavigationLink(destination: ProfileInformationForSubscriber(username: action.organizer)){
                        HStack{
                            Image(systemName: "person")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                            Text(action.organizer)
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                        }
                        .padding([.leading, .trailing], 22)
                        .font(.headline)
                    }
                    // Darstellen des Datums und der Uhrzeit der Aktion
                    HStack{
                        Image(systemName: "calendar")
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                        Text(action.date, style: .date)
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                        Text(action.date, style: .time)
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                    }
                    .padding([.leading, .trailing], 22)
                    .font(.headline)
                    // Darstellen der Location der Aktion
                    HStack{
                        Image(systemName: "mappin.and.ellipse")
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                        Text(action.location)
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                    }
                    .padding([.leading, .trailing], 22)
                    .font(.headline)
                    // Darstellen, wie viele Personen angemeldet sind
                    HStack{
                        Image(systemName: "person.3.fill")
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                        Text("\(action.subscriber.count)")
                            .foregroundStyle(Color.accentGreen)
                            .brightness(-0.2)
                    }
                    .padding([.leading, .trailing], 22)
                    .font(.headline)
                    // Beschreibung der Aktion
                    VStack(alignment: .leading, spacing: 15){
                        Text("Beschreibung")
                            .foregroundStyle(.secondary)
                            .font(.body)
                        
                        Text(action.description)
                            .font(.body)
                    }
                    .padding([.leading, .trailing], 22)
                    .font(.headline)
                    
                    VStack(alignment: .leading, spacing: 15){
                        Text("Folgende Sachen werden benötigt:")
                            .foregroundStyle(.secondary)
                            .font(.body)
                        Text(action.items)
                            .font(.body)
                    }
                    .padding([.leading, .trailing], 22)
                    .font(.headline)
                    
                    Spacer()
                    // Unterscheidung, ob der Benutzer der Aktion beigetreten ist, dann sollen jeweils andere Buttons erscheinen
                    if action.subscriber.contains(dataManager.uid){
                        Button(action: {
                            // Aktueller Benutzer meldet sich ab
                            dataManager.removeSubscriber(uid: dataManager.uid, actionId: self.actionID) { error in
                                if let error = error {
                                    alertMessage = "Fehler beim aktualiseren: \(error.localizedDescription)"
                                } else {
                                    alertMessage = "Erfolgreich abgemeldet!"
                                }
                                showAlert = true
                            }
                            //Zurück gehen zum ParentView und onBack-Funktionen ausführen
                            presentationMode.wrappedValue.dismiss()
                            onBack()

                        }, label: {
                            Text("Du bist schon dabei! Abmelden?")
                                .bold()
                                .foregroundStyle(.black)
                                .brightness(-0.2)
                                .frame(width: 340, height: 40)
                                .background(
                                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                                        .foregroundStyle(.accentGreen)
                                )
                        })
                        .padding([.leading, .trailing], 22)
                        .font(.headline)
                        .offset(x: 2)

                    } else {
                        Button(action: {
                            // Aktueller Benutzer meldet sich an
                            dataManager.addSubscriber(uid: dataManager.uid, actionId: self.actionID) { error in
                                if let error = error {
                                    alertMessage = "Fehler beim aktualiseren: \(error.localizedDescription)"
                                } else {
                                    alertMessage = "Erfolgreich angemeldet!"
                                }
                                showAlert = true
                            }
                            //Zurück gehen zum ParentView und onBack-Funktionen ausführen
                            presentationMode.wrappedValue.dismiss()
                            onBack()
                        }) {
                            Text("Aktion beitreten")
                                .bold()
                                .foregroundStyle(.black)
                                .brightness(-0.2)
                                .frame(width: 340, height: 40)
                                .background(
                                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                                        .foregroundStyle(.accentGreen)
                                )}
                        .padding([.leading, .trailing], 22)
                        .font(.headline)
                        .offset(x: 2)
                    }
                    
                    Spacer()
                    
                    
                }
                .navigationTitle("Details")
            }
        }
    }
}
