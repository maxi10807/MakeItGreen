//
//  ActionCard.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 16.06.24.
//

import SwiftUI

struct ActionCard: View {
    // Deklarieren der benötigten Variablen um die ActionCard zu füllen
    let actionHeading: String
    let actionCategorie: String
    let actionOrganizer: String
    let actionImage: String
    let actionLocation: String
    let actionSubcriberCount: Int
    
    var body: some View {
            VStack{
                Image(actionImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                
                HStack {
                    VStack(alignment: .leading, spacing: 8){
                        // Aktions-Kategorie
                        Text(actionCategorie)
                            .font(.headline)
                            .foregroundColor(Color.accentGreen)
                            .brightness(-0.3)
                        // Aktions-Name
                        Text(actionHeading)
                            .font(.title)
                            .fontWeight(.black)
                            .foregroundStyle(Color.primary)
                            .lineLimit(3)
                            .multilineTextAlignment(.leading)
                        // Aktions Organisator
                        HStack {
                            Image(systemName: "person.fill")
                                .foregroundStyle(.black)
                            Text(actionOrganizer)
                                .font(.title3)
                                .foregroundStyle(.black)
                        }
                        // Aktions-Location
                        HStack {
                            Image(systemName: "mappin.and.ellipse")
                                .foregroundStyle(.black)
                            Text(actionLocation)
                                .multilineTextAlignment(.leading)
                                .font(.title3)
                                .foregroundStyle(.black)
                            Spacer()
                            Image(systemName: "person.fill")
                                .bold()
                                .font(.title)
                            Text("\(actionSubcriberCount)")
                                .font(.title)
                        }
                    }
                    .layoutPriority(100)
                    Spacer()
                }
                .padding()
            }
        // Die Daten sollen in einer Karte angezeigt werden, mit einem grünen Rand
            .cornerRadius(10)
            .overlay{
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.accentGreen, lineWidth: 2)
            }
        .padding([.top, .horizontal])
        }
}

