//
//  LoginView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 23.06.24.
//

import SwiftUI

struct LoginView: View {
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Variablen für den Login
    @State private var email: String = ""
    @State private var password: String = ""
    
    // Anpassung der Textfelder
    init(){
        UITextField.appearance().tintColor = UIColor.accentGreen
        UITextView.appearance().tintColor = UIColor.accentGreen
    }
    
    var body: some View {
        NavigationStack {
            // gleicher Hintergrund wie beim RegisterView
            ZStack {
                LinearGradient.linearGradient(colors: [.white, .green], startPoint: .bottomTrailing, endPoint: .topLeading)
                
                RoundedRectangle(cornerRadius: 30, style: .continuous)
                    .foregroundStyle(.linearGradient(colors: [.accentGreen, .white], startPoint: .topLeading, endPoint: .bottomTrailing))
                    .frame(width: 1000, height: 400)
                    .rotationEffect(.degrees(135))
                    .offset(y: -350)
                
                VStack(spacing: 20){
                    // Überschrift für Login
                    VStack(alignment: .leading) {
                        Text("Willkommen")
                            .foregroundStyle(.black)
                            .font(.largeTitle).bold()
                            .offset(y: -100)
                        Text("Make It Green")
                            .foregroundStyle(.black)
                            .font(.largeTitle).bold()
                            .offset(y: -100)
                    }
                    .offset(x: -60)
                    // Eingabe der Datebn für ein Login
                    TextField("Email", text: $email)
                        .foregroundStyle(.black)
                        .textFieldStyle(.roundedBorder)
                        .bold()
                        .autocapitalization(.none)
                    
                    SecureField("Passwort", text: $password)
                        .foregroundStyle(.black)
                        .textFieldStyle(.roundedBorder)
                        .bold()
                        .autocapitalization(.none)
                    // Falls es eine Fehlermeldung gibt soll diese angezeigt werden
                    if let errorMessage = dataManager.errorMessage {
                        Text(errorMessage)
                            .foregroundColor(.red)
                            .bold()
                            .padding(.top, 10)
                    }
                    
                    // Button für Login-Funktion
                    Button {
                        dataManager.login(email: email, password: password)
                    } label: {
                        Text("Anmelden")
                            .bold()
                            .frame(width: 320, height: 40)
                            .foregroundStyle(.accentGreen)
                            .brightness(-0.2)
                            .background(
                                RoundedRectangle(cornerRadius: 10, style: .continuous)
                            )
                    }
                    .padding(.top)
                    .offset(y: 110)
                    
                    // Link zum RegisterView
                    HStack{
                        NavigationLink(destination: RegisterView()) {
                            Text("Noch kein Account? Registrieren")
                                .foregroundStyle(.black.secondary)
                        }
                    }
                    .padding(.top)
                    .offset(y: 110)
                    }
                    .frame(width: 350)
                }
                .ignoresSafeArea()
            }
        }
    }


#Preview {
    LoginView()
        .environmentObject(DataManager())
}
