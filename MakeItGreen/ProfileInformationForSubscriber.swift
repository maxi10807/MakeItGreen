//
//  ProfileInformationForSubscriber.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 06.07.24.
//

import SwiftUI
// Fast die selbe View wie die ProfileView nur ohne Signout-Button, damit Benutzer Informationen über den Organisator bekommen, um ihn z.B. zu kontaktieren
struct ProfileInformationForSubscriber: View {
    @Environment(\.dismiss) var dismiss
    var username: String
    var profileImage: String = "profile"
    @EnvironmentObject var dataManager: DataManager
    
    var body: some View {
        VStack{
            Spacer()
                .navigationBarBackButtonHidden(true)
                .toolbar(content: {
                    ToolbarItem(placement: .topBarLeading) {
                        Button(action: {
                            dismiss()
                        }, label: {
                            Image(systemName: "arrow.left")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                            Text("Zurück")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                        })
                    }
                })
        }
        if let user = dataManager.users.first(where: { username == $0.username }){
            VStack(spacing: 20){
                Image(profileImage)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.accentGreen, lineWidth: 4))
                    .shadow(radius: 10)
                
                // Name
                Text(user.username)
                    .font(.title)
                    .fontWeight(.bold)
                
                // Biografie
                Text(user.bio)
                    .font(.body)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal)
                
                // Standort
                HStack {
                    Image(systemName: "location.fill")
                        .foregroundColor(.accentGreen)
                    Text(user.location)
                        .foregroundColor(.accentGreen)
                }
                .brightness(-0.2)
                
                VStack(spacing: 10) {
                    HStack {
                        Image(systemName: "envelope.fill")
                            .foregroundColor(.accentGreen)
                        Text(user.email)
                            .foregroundColor(.accentGreen)
                    }
                    .brightness(-0.2)
                    HStack {
                        Image(systemName: "phone.fill")
                            .foregroundColor(.accentGreen)
                        Text(user.tel)
                            .foregroundColor(.accentGreen)
                    }
                    .brightness(-0.2)
                }

                Spacer()
            }
        }
    }
}

