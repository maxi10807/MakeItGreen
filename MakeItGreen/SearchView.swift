//
//  SwiftUIView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 11.06.24.
//

import SwiftUI

struct SearchView: View {
    // Variable für die gesuchte Überschrift
    @State private var searchText = ""
    // EInbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Variablen für die Fehlermeldung
    @State private var alertMessage = ""
    @State private var showAlert = false

    // Filterung der Daten
    var filteredData: [Action] {
        // Wenn gesuchter Text ist leer, dann sollen die Daten unverändert zurückgegeben werden
        if searchText.isEmpty {
            return dataManager.actions
            // Sonst soll nach dem Objekt mit der Überschrift, welche der Suchtext beinhaltet geuscht werden
        } else {
            return dataManager.actions.filter { $0.heading.localizedCaseInsensitiveContains(searchText) }
        }
    }
    var body: some View {
        NavigationStack() {
            ScrollView() {
            // Erzeugen der ActionCards
                ForEach(filteredData, id: \.id) { action in
                    // Link für die ActionView mit passenden Daten
                    NavigationLink(destination: ActionView(actionID: action.id, onBack: {
                        // Bei Rückkehr zur ParentView sollen folgende Funktionen ausgeführt werden
                        dataManager.fetchUsers()
                        dataManager.fetchActions()
                        // Das passende Alert soll ausgegeben werden
                        alertMessage = action.subscriber.contains(dataManager.uid) ? "Erfolgreich abgemeldet!" : "Erfolgreich angemeldet!"
                        showAlert = true
                    })){
                        // Daten für die ActionCard
                        ActionCard(actionHeading: action.heading, actionCategorie: action.categorie, actionOrganizer: action.organizer, actionImage: action.image, actionLocation: action.location, actionSubcriberCount: action.subscriber.count)
                            .opacity(action.isPastTime() ? 0.3 : 0.8)
                    }
                }
            }
            .navigationTitle("Veranstaltungen")
        }
        // Beim erscheinen der View sollen die Daten aktualisert werden
        .onAppear {
            dataManager.fetchActions()
        }
        // Es soll nach dem Suchtext gesucht werden
        .searchable(text: $searchText)
        .tint(Color.accentGreen)
        // Passende Fehlermeldung
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text("Status"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
        })
    }
}

#Preview {
    SearchView()
        .environmentObject(DataManager())
}


