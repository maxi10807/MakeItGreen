//
//  TipView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 17.06.24.
//

import SwiftUI

struct TipView: View {
    // Einbinden der Dismiss Umgebung, damit man aus der TipView wieder zur HomeView gelangt
    @Environment(\.dismiss) var dismiss
    // Einbinden des DataManagers
    @EnvironmentObject var dataManager: DataManager
    // Deklarieren der Variable für die TIPID
    var tipID: String
    
    var body: some View {
        // Erstellen des Zurück-Buttons, welcher dann wieder zur ParentView führt
        VStack(){
            Spacer()
            .navigationBarBackButtonHidden(true)
                .toolbar(content: {
                    ToolbarItem(placement: .topBarLeading) {
                        Button(action: {
                            // Führt die Dismiss-Funktion aus
                            dismiss()
                        }, label: {
                            Image(systemName: "arrow.left")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                            Text("Zurück")
                                .foregroundStyle(Color.accentGreen)
                                .brightness(-0.2)
                                .bold()
                        })
                    }
                })
        }
        // Deklarieren des Objektes aus dem Array, welches die selbe TipID hat
        if let tip = dataManager.tips.first(where: { $0.id == tipID }) {
            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    // Darstellen der Überschrift
                    Text(tip.heading)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding([.leading, .trailing], 16)
                        .foregroundStyle(.accentGreen)
                        .brightness(-0.2)
                    // Darstellen des Bilde
                    Image(tip.image)
                        .resizable()
                        .frame(height: 220)
                        .cornerRadius(10)
                        .padding([.leading, .trailing], 16)
                    // Darstellen des Textes
                    Text(tip.text)
                        .font(.body)
                        .padding(22)
                    // Darstellen des Links, von wo die Tips stammen
                    HStack{
                        Image(systemName: "link")
                            .foregroundStyle(.accentGreen)
                            .brightness(-0.2)
                        // Link zur Webseite
                        Link("MSC - mehr Nachhaltigkeit", destination: URL(string: "https://www.msc.org/de/blog/mehr-nachhaltigkeit-tipps")!)
                            .font(.body)
                            .foregroundStyle(.accentGreen)
                            .brightness(-0.2)
                        Spacer()
                    }
                    .padding([.leading, .trailing], 22)
                }
            }
            .navigationTitle("Tipp")
        }
    }
}

