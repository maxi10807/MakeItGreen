//
//  Action.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 20.06.24.
//

import SwiftUI

// Hier werden die benötigten Daten für ein Aktions-Objekt deklariert
struct Action: Identifiable, Hashable, Codable{
    var id: String
    var heading: String
    var organizer: String
    var categorie: String
    var image: String
    var location: String
    var date: Date
    var items: String
    var description: String
    var subscriber: [String] = []
}

// Diese Extension prüft, ob eine Aktion schon nach der aktuellen Zeit ist
extension Action {
    func isPastTime() -> Bool {
        let currentTime = Date()
        return currentTime > date
    }
}
