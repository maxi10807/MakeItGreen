//
//  MainView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 23.06.24.
//

import SwiftUI

struct MainView: View {
    // Einbinden des DataManagers
    @StateObject private var dataManager = DataManager()
    
    var body: some View {
        // Wenn der Benutzer eingeloggt ist, soll die ContentView erscheinen, sonst soll der Benutzer zur LoginView geführt werden
        Group {
            if dataManager.isLoggedIn {
                ContentView()
            } else {
                LoginView()
            }
        }
        .onAppear {
            // Wenn die App gestartet wird soll die Funktion Listen ausgeführt werden.
            dataManager.listen()
        }
    }
}

#Preview {
    MainView()
}
