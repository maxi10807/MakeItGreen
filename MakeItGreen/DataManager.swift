//
//  SwiftUIView.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 20.06.24.
//

import SwiftUI
import Firebase
import FirebaseAuth
import FirebaseFirestore

// Definieren der Klasse DataManager
class DataManager: ObservableObject {
    // Array für Tipps
    @Published var tips: [Tip] = []
    // Array für Aktionen
    @Published var actions: [Action] = []
    // Array für User
    @Published var users: [User] = []
    // Array für Meine Aktionen
    @Published var myActions: [Action] = []
    // Überprüfung ob der Benutzer eingeloggt ist
    @Published var isLoggedIn: Bool = false
    // Variable für die UID des aktuellen Benutzers
    @Published var uid: String = ""
    private var handle: AuthStateDidChangeListenerHandle?
    private var initActionsArray: [String] = []
    // Variable für Fehlermeldung
    @Published var errorMessage: String?
    
    // Beim Deklarieren soll die Funktion Listen ausgeführt werden
    init() {
        listen()
    }
    
    // Es soll die CurrentUID gefetched werden und danahc sollen die Funktionen ausgeführt werden, sonst sollen alle variablen/ arrays cleared werden
    func fetchCurrentUserUID(){
        if let currentUser = Auth.auth().currentUser {
            self.uid = currentUser.uid
            fetchTips()
            fetchUsers()
            fetchActions()
            fetchMyActions()
        } else {
            self.uid = ""
            self.tips.removeAll()
            self.users.removeAll()
            self.actions.removeAll()
            self.myActions.removeAll()
        }
    }
    
    // Hier sollen die Tipps gefetched werden
    func fetchTips() {
        // Alle vorhandenen Einträge löschen
        tips.removeAll()
        // Firestoreverbindung aufbauen und passende Collection auswählen
        let db = Firestore.firestore()
        let ref = db.collection("tips")
        // Snapshot machen oder Fehlermeldung ausgeben
        ref.getDocuments { snapshot, error in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            if let snapshot = snapshot {
                // Für alle Dokumente im Snapshot
                for document in snapshot.documents {
                    // Daten aus dem document sollen data sein
                    let data = document.data()
                    
                    // die Daten aus dem Dokument als Variable setzen
                    let id = data["id"] as? String ?? ""
                    let heading = data["heading"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    let text = data["text"] as? String ?? ""
                    let image = data["image"] as? String ?? ""
                    
                    // Objekt erstellen
                    let tip = Tip(id: id, heading: heading, description: description, text: text, image: image)
                    // Array anhängen
                    self.tips.append(tip)
                }
            }
        }
        print("Tips fetched")
    }
    
    // Benutzer zur Aktion hinzufügen
    func addSubscriber(uid: String, actionId: String, completion: @escaping (Error?) -> Void) {
        // Firestore verbindung
        let db = Firestore.firestore()
        let ref = db.collection("actions").document(actionId)
        let refUser = db.collection("users").document(uid)
        // Array subcriber updaten mit UID
        ref.updateData([
            "subscriber": FieldValue.arrayUnion([uid])
        ]) { error in
            if let error = error {
                print("Error writing UID to actions.subscriber: \(error.localizedDescription)")
                completion(error)
            } else {
                print("UID successfully written!")
                completion(nil)
            }
        }
        // Array action updaten mit ActionID
        refUser.updateData([
            "actions": FieldValue.arrayUnion([actionId])
        ]) { error in
            if let error = error {
                print("Error writing action to users.actions: \(error.localizedDescription)")
                completion(error)
            } else {
                print("ActionID successfully written!")
                completion(nil)
            }
        }
    }
    
    // Benutzer von Aktion entfernen
    func removeSubscriber(uid: String, actionId: String, completion: @escaping (Error?) -> Void) {
        // Firestore Verbindung
        let db = Firestore.firestore()
        let ref = db.collection("actions").document(actionId)
        let refUser = db.collection("users").document(uid)
        
        // Aktions-Dokuement fangen
        ref.getDocument{ document, error in
            guard let document = document, document.exists, error == nil else {
                print("Error fetching action document: \(error?.localizedDescription ?? "Unknown error")")
                completion(error)
                return
            }
            
            // Benötigte Daten als Variablen deklarieren
            let data = document.data()
            let organizer = data?["organizer"] as? String ?? ""
            // User-Dokument fangen
            refUser.getDocument{ userDocument, error in
                guard let userDocument = userDocument, userDocument.exists, error == nil else {
                    print("Error fetching user document: \(error?.localizedDescription ?? "Unknown error")")
                    completion(error)
                    return
                }
                // Benötigte Daten als Variablen deklarieren
                let userData = userDocument.data()
                let username = userData?["username"] as? String ?? ""
                
                // Wenn der Organisator gleich dem aktuellen Benutzer ist, kann dieser sich nicht aus der Aktion ausschreiben
                if organizer == username {
                    print("Cannot remove subscriber: User is the organizer")
                    completion(NSError(domain: "", code: 403, userInfo: [NSLocalizedDescriptionKey: "Du kannst dich nicht abmelden, da du der Organisator dieser Aktion bist."]))
                    return
                }
                
                // Sonst sollen die passende UID und die passende actionID von den Arrays entfernt werden
                ref.updateData([
                    "subscriber": FieldValue.arrayRemove([uid])
                ]) { error in
                    if let error = error {
                        print("Error removing UID from actions.subscriber: \(error.localizedDescription)")
                        completion(error)
                    } else {
                        print("UID successfully removed!")
                        completion(nil)
                    }
                }
                refUser.updateData([
                    "actions": FieldValue.arrayRemove([actionId])
                ]) { error in
                    if let error = error {
                        print("Error removing action from users.actions: \(error.localizedDescription)")
                        completion(error)
                    } else {
                        print("ActionID successfully removed!")
                        completion(nil)
                    }
                }
            }
        }
    }
    
    // Hier sollen alle Aktionen gefetched werden
    func fetchActions() {
        // Alle vorhandenen Einträge löschen
        actions.removeAll()
        // Firestore Verbindung
        let db = Firestore.firestore()
        let ref = db.collection("actions")
        // alle Dokumente der Collection fetchen
        ref.getDocuments { snapshot, error in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            if let snapshot = snapshot {
                // Für jedes Dokument
                    for document in snapshot.documents {
                        // soll eine Variable Data deklariert werden
                        let data = document.data()
                        // die Daten aus dem Dokument als Variable setzen
                        let id = data["id"] as? String ?? ""
                        let heading = data["heading"] as? String ?? ""
                        let organizer = data["organizer"] as? String ?? ""
                        let image = data["image"] as? String ?? ""
                        let categorie = data["categorie"] as? String ?? ""
                        let location = data["location"] as? String ?? ""
                        let timestamp = data["date"] as? Timestamp ?? Timestamp()
                        let items = data["items"] as? String ?? ""
                        let desc = data["description"] as? String ?? ""
                        let subscriber = data["subscriber"] as? [String] ?? []
                        let date = timestamp.dateValue()
                        
                        // Objekt erzeugen
                        let action = Action(id: id, heading: heading, organizer: organizer, categorie: categorie, image: image, location: location, date: date, items: items, description: desc, subscriber: subscriber)
                        // Objekt dem Array anhängen
                        self.actions.append(action)
                    }
                }
        }
        print("Actions fetched")
    }
    
    // Heri passiert fast das selbe wie in der Funktion fetchActions()
    func fetchMyActions() {
        myActions.removeAll()
        print("Actions fetched for \(uid)")
        let db = Firestore.firestore()
        let ref = db.collection("actions")
        ref.getDocuments { snapshot, error in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            if let snapshot = snapshot {
                    for document in snapshot.documents {
                        let data = document.data()
                        
                        let id = data["id"] as? String ?? ""
                        let heading = data["heading"] as? String ?? ""
                        let organizer = data["organizer"] as? String ?? ""
                        let image = data["image"] as? String ?? ""
                        let categorie = data["categorie"] as? String ?? ""
                        let location = data["location"] as? String ?? ""
                        let timestamp = data["date"] as? Timestamp ?? Timestamp()
                        let items = data["items"] as? String ?? ""
                        let desc = data["description"] as? String ?? ""
                        let subscriber = data["subscriber"] as? [String] ?? []
                        let date = timestamp.dateValue()
                        
                        let action = Action(id: id, heading: heading, organizer: organizer, categorie: categorie, image: image, location: location, date: date, items: items, description: desc, subscriber: subscriber)
                        // Wenn das Array "Subscriber" die UID des aktuellen Benutzers enthält, nur dann soll die Aktion dem MyActions-Array angehängt werden
                        if action.subscriber.contains(self.uid) {
                            self.myActions.append(action)
                        }
                    }
                }
        }
    }
    
    // Hier sollen die User gefetched werden
    func fetchUsers() {
        // Array clearen
        users.removeAll()
        // Verbindugn zum Firestore
        let db = Firestore.firestore()
        let ref = db.collection("users")
        // Snapshot
        ref.getDocuments { snapshot, error in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            if let snapshot = snapshot {
                // Für jedes Dokument
                for document in snapshot.documents {
                    // soll data die Daten des Dokumentes sein
                    let data = document.data()
                    
                    // Daten als Variablen deklarieren
                    let id = data["id"] as? String ?? ""
                    let username = data["username"] as? String ?? ""
                    let bio = data["bio"] as? String ?? ""
                    let location = data["location"] as? String ?? ""
                    let email = data["email"] as? String ?? ""
                    let tel = data["tel"] as? String ?? ""
                    let actions = data["actions"] as? [String] ?? []
                    
                    // Objekt erzeugen
                    let user = User(id: id, username: username, bio: bio, tel: tel, location: location, email: email, actions: actions)
                    // dem Array Users anhängen
                    self.users.append(user)
                }
            }
        }
    }
    
    
    // neue Aktion hinzufügen
    func addAction(action: Action, completion: @escaping (Error?) -> Void) {
        // Firestore Verbindung
        let db = Firestore.firestore()
        do {
            // ActionID = Dokumenten ID
            let documentReference = db.collection("actions").document(action.id)
            // Aktionsobjekt speichern
            try documentReference.setData(from: action) { error in
                if let error = error {
                    print("Error writing event to Firestore: \(error.localizedDescription)")
                    completion(error)
                } else {
                    print("Event successfully written!")
                    completion(nil)
                }
            }
        } catch let error {
            print("Error encoding event: \(error.localizedDescription)")
            completion(error)
        }
    }
    
    // Registrierung neuer Benutzer
    func register(email: String, password: String, username: String, tel: String, location: String, bio: String) {
        // Erstellen des Users in Firestore
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if error != nil {
                print(error!.localizedDescription)
            }
            guard let user = authResult?.user else { return }
            
            let db = Firestore.firestore()
            // Firestore Verbindung und speichern der Daten des Users
            db.collection("users").document(user.uid).setData([
                "id": user.uid,
                "username": username,
                "email": email,
                "tel": tel,
                "location": location,
                "bio": bio,
                "actions": self.initActionsArray
            ])
            print("Register successfully!")
        }
    }
    
    // Login eine Users
    func login(email: String, password: String) {
        // Firebase signIn-Funktion
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if let error = error {
                self.errorMessage = "Email-Adresse oder Passwort falsch!"
                print(error.localizedDescription)
            } else {
                self.errorMessage = nil
                print("Login successfully!")
                print("UID set to \(self.uid)")
                }
            }
        }
    
    // Funktion für den An- und Abmeldezustand
    func listen() {
        // Hinzufügen eines Listeners
        handle = Auth.auth().addStateDidChangeListener { auth, user in
            // wenn ein Benutzer angemeldet ist
            if let _ = user {
                // setzen bzw. Auführen von Befehlen
                self.isLoggedIn = true
                self.fetchCurrentUserUID()
                print("User already logged in!")
                print("UID is \(self.uid)")
            } else {
                // Sonst alle Daten löschen
                self.isLoggedIn = false
                self.uid = ""
                self.tips.removeAll()
                self.users.removeAll()
                self.actions.removeAll()
                self.myActions.removeAll()
            }
        }
    }
    
    // Funktion zum Abmelden
    func signOut() {
        do {
            // Firebase Funktion
            try Auth.auth().signOut()
            // Löschen aller Daten
            self.uid = ""
            self.tips.removeAll()
            self.users.removeAll()
            self.actions.removeAll()
            self.myActions.removeAll()
            print("User signed out and data cleared.")
        } catch {
            print("Error signing out: \(error.localizedDescription)")
        }
    }
}
