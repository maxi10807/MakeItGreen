//
//  User.swift
//  MakeItGreen
//
//  Created by Maximilian Einhaus on 24.06.24.
//

import SwiftUI

//Deklarieren der Variablen des Objektes User
struct User: Identifiable, Codable{
    var id: String
    var username: String
    var bio: String
    var tel: String
    var location: String
    var email: String
    var actions: [String]
}



